# Micronik Skeleton

..is a Light- Mediumweight Skeleton to build WP Themes with. This is done because I couldn't really find my way through all the tutorials or tools that were on the web. Some didn't have understandable info or it just was missing...


## License:
License: MIT
License URI: https://gitlab.com/vtxWP/micronik/blob/master/LICENSE


### Tags:
one-column, two-columns, right-sidebar, flexible-width, custom-header, custom-menu, editor-style, featured-images, microformats, post-formats, rtl-language-support, sticky-post, translation-ready
